// specs.h -- Forecast specifications.
//
// Copyright 2020 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SPECS_H
#define SPECS_H

#include <stdexcept>

#include <cosmox/numeric.h>
#include <cosmox/survey_specs.h>

namespace tayf {

  /** @brief Euclid spec-z galaxy bias. */
  class EuclidBias: public cosmox::GalaxyBias {
  public:
    double
    b1 (double z)
    {
      check_range(z);
      return 0.4*z + 0.9;
    }

    double
    b2 (double z)
    {
      check_range(z);
      return (-0.00771288*pow(z, 3.) + 0.18302286*pow(z, 2.) - 0.20799274*z -
              0.70417186);
    }

    double
    bS (double z)
    {
      return -2./7.*(b1(z)-1.);
    }

  private:
    void check_range(double z)
    {
      if ((z<0.7) || (z>2.0))
        throw std::runtime_error("Galaxy bias out of range");
    }
  };

  /** @brief Galaxy bias based on fitting formulas and Lagrangian LIMD.
   *
   *  See table 7 used in arXiv:1611.09787 forecasts.
   *
   *  @warning In this prototype we fix arbitrarily b1=1.5.
   */
  class DjsBias: public cosmox::GalaxyBias {
  public:
    DjsBias(const double b1): kb1(b1)
    {
    }

    double
    b2 (double z)
    {
      auto b1_z = b1(z);
      if ((b1_z<1) || (b1_z > 9))
        throw_outofrange();
      return 0.412 - 2.143*b1_z + 0.929*pow(b1_z, 2) + 0.008*pow(b1_z, 3);
    }

    double
    bS (double z)
    {
      return -2./7.*(b1(z)-1.);
    }

  private:
    double kb1;

    void throw_outofrange()
    {
      throw std::runtime_error("Galaxy bias out of range");
    }

    double
    b1 (double z)
    {
      return 1.5;
    }
  };

  struct WindowParams {
    // Define tophat in photometric redshift.
    double zp_min;
    double zp_max;
  };

  double
  window_itegrand(double z, void* params)
  {
    WindowParams* pars = static_cast<WindowParams*>(params);
    auto sig = 0.05*(1 + 0.5*(pars->zp_min+pars->zp_max));
    auto z0 = 0.9/std::sqrt(2);
    auto dNdz = std::pow(z/z0, 2.) * std::exp(-pow(z/z0, 3./2.));
    return dNdz * (std::erf((pars->zp_max - z)/(std::sqrt(2)*sig))
                   - std::erf((pars->zp_min - z)/(std::sqrt(2)*sig)));
  }

  /** @brief Photo-z window function */
  class WindowEuclidPhoto: public cosmox::Window {
  public:
    WindowEuclidPhoto(void* params,
                      const double zmin,
                      const double zmax):
      cosmox::Window(zmin, zmax), params(params)
    {
      cosmox::GslIntegrator integrator;
      auto res = integrator.qag(&window_itegrand, zmin, zmax, params);
      norm = res.integral;
    }

    double
    eval(const double z)
    {
      return window_itegrand(z, params) / norm;
    }

  private:
    double norm;
    void* params;
  };

  /** @brief Tophat window function */
  class WindowSpec: public cosmox::Window {
  public:
    using Window::Window;

    double
    eval(const double z)
    {
      return 1/(get_zmax()-get_zmin());
    }
  };
} /* namespace tayf */

#endif  /* SPECS_H */
