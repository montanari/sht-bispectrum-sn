// sn.h -- Signal and noise computation.
//
// Copyright 2020 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SN_H
#define SN_H

#include <iostream>
#include <string>
#include <vector>

#include <cosmox/bispectrum_density.h>
#include <cosmox/general_spectra.h>
#include <cosmox/perturbations.h>
#include <cosmox/special_functions.h>

#include "specs.h"
#include "utils.h"

namespace tayf {
  class SignalVariance {
  public:
    SignalVariance(const std::string inifile,
                   const std::shared_ptr<cosmox::Perturbations> pt,
                   const std::shared_ptr<cosmox::GalaxyBias> galbias,
                   const std::shared_ptr<cosmox::GeneralSpectra> gsp,
                   const std::shared_ptr<cosmox::Wigner> wig,
                   const bool linear_rsd,
                   const double shot_noise):
      inifile(inifile),
      pt(pt),
      galbias(galbias),
      gsp(gsp),
      wig(wig),
      linear_rsd(linear_rsd),
      shot_noise(shot_noise)
    {
      bdens = std::make_shared<cosmox::BispectrumDensity>(gsp,
                                                          wig,
                                                          linear_rsd,
                                                          epsrel,
                                                          epsabs,
                                                          integtype);
    }

    void
    compute(const std::vector<std::vector<int>> triangles, const double zz)
    {
      std::vector<double> signal(triangles.size(), NAN);
      std::vector<double> variance(triangles.size(), NAN);
      std::cout << "# l1  l2  l3  signal  variance\n";
#pragma omp parallel for
      for (size_t i=0; i<triangles.size(); ++i)
        {
          auto l1 = triangles.at(i).at(0);
          auto l2 = triangles.at(i).at(1);
          auto l3 = triangles.at(i).at(2);
          signal.at(i) = bdens->compute(l1, l2, l3, zz, zz, zz);
          variance.at(i) = bdens->compute_variance(l1, l2, l3,
                                                   zz, zz, zz,
                                                   shot_noise);
          tayf::print_sn(l1, l2, l3, signal.at(i), variance.at(i));
        }
    }

  protected:
    std::string inifile;
    std::shared_ptr<cosmox::Perturbations> pt;
    std::shared_ptr<cosmox::GalaxyBias> galbias;
    std::shared_ptr<cosmox::GeneralSpectra> gsp;
    std::shared_ptr<cosmox::Wigner> wig;
    bool linear_rsd;
    double shot_noise;

  private:
    double epsrel = 1e-3;
    double epsabs = 0.;
    cosmox::GeneralSpectra::IntegType integtype = cosmox::GeneralSpectra::kCquad;
    std::shared_ptr<cosmox::BispectrumDensity> bdens;
  };

  class SignalVarianceW: public SignalVariance {
  public:
    SignalVarianceW(const std::string inifile,
                    const std::shared_ptr<cosmox::Perturbations> pt,
                    const std::shared_ptr<cosmox::GalaxyBias> galbias,
                    const std::shared_ptr<cosmox::GeneralSpectra> gsp,
                    const std::shared_ptr<cosmox::Wigner> wig,
                    const bool linear_rsd,
                    const double shot_noise):
      SignalVariance(inifile, pt, galbias, gsp, wig, linear_rsd, shot_noise)
    {
      bdens = std::make_shared<cosmox::BispectrumDensityW>(gsp,
                                                           wig,
                                                           linear_rsd,
                                                           epsrel,
                                                           epsabs);
    }

    void
    compute(const std::vector<std::vector<int>> triangles,
            const std::shared_ptr<cosmox::Window> win)
    {
      std::vector<double> signal(triangles.size(), NAN);
      std::vector<double> variance(triangles.size(), NAN);
      std::cout << "# l1  l2  l3  signal  variance\n";
      for (size_t i=0; i<triangles.size(); ++i)
        {
          auto l1 = triangles.at(i).at(0);
          auto l2 = triangles.at(i).at(1);
          auto l3 = triangles.at(i).at(2);
          try
            {
              signal.at(i) = bdens->compute(l1, l2, l3, win, win, win);
              variance.at(i) = bdens->compute_variance(l1, l2, l3,
                                                       win, win, win,
                                                       shot_noise);
            }
          catch (const cosmox::CosmoxException& e)
            {
            }
          tayf::print_sn(l1, l2, l3, signal.at(i), variance.at(i));
        }
    }

  private:
    double epsrel = 1e-1;
    double epsabs = 0.;
    std::shared_ptr<cosmox::BispectrumDensityW> bdens;

  };
} /* namespace tayf */

#endif  /* SN_H */
