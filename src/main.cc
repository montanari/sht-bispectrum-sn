// main.cc -- compute bispectrum signal and variance
//
// Copyright 2020 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include<iostream>
#include<sstream>

#include "sn.h"
#include "specs.h"
#include "utils.h"

enum WindowType
  {
   kNoLow,
   kNoHigh,
   kPhotoLow,
   kPhotoHigh,
   kSpecLow,
   kSpecHigh,
   kSpecHighNarrow,
  };

void
run_nowindow(const std::string inifile,
             const std::shared_ptr<cosmox::Perturbations> pt,
             const std::shared_ptr<cosmox::Wigner> wig,
             const bool linear_rsd,
             const int lmax,
             const WindowType wtype)
{
  double zz;
  switch (wtype)
    {
    case WindowType::kNoLow:
      zz = 0.49;
      break;
    case WindowType::kNoHigh:
      zz = 0.96;
      break;
    default:
      throw tayf::TayfException("Not implemented", __FILE__, __LINE__);
    }
  auto shot_noise = 0.;
  auto galbias = std::make_shared<cosmox::GalaxyBias>();
  auto gsp = std::make_shared<cosmox::GeneralSpectra>(pt, galbias);
  tayf::SignalVariance sn(inifile, pt, galbias, gsp, wig, linear_rsd,
                          shot_noise);
  auto triangles = tayf::get_triangles(3, lmax);

  std::cout << "# z=" << zz << std::endl;
  std::cout << "# shot_noise=" << shot_noise << std::endl;
  std::cout << "#" << std::endl;
  sn.compute(triangles, zz);
}

void
run_photo(const std::string inifile,
          const std::shared_ptr<cosmox::Perturbations> pt,
          const std::shared_ptr<cosmox::Wigner> wig,
          const bool linear_rsd,
          const int lmax,
          const WindowType wtype)
{
  tayf::WindowParams params;
  std::shared_ptr<tayf::WindowEuclidPhoto> win;
  switch (wtype)
    {
    case WindowType::kPhotoLow:
      params.zp_min = 0.42;
      params.zp_max = 0.56;
      win = std::make_shared<tayf::WindowEuclidPhoto>(&params, 0.2, 1.0);
      break;
    case WindowType::kPhotoHigh:
      params.zp_min = 0.90;
      params.zp_max = 1.02;
      win = std::make_shared<tayf::WindowEuclidPhoto>(&params, 0.54, 1.4);
      break;
    default:
      throw tayf::TayfException("Not implemented", __FILE__, __LINE__);
    }
  auto shot_noise = 2.82e-8;
  auto galbias = std::make_shared<tayf::DjsBias>(1.5);
  auto gsp = std::make_shared<cosmox::GeneralSpectra>(pt, galbias);
  tayf::SignalVarianceW sn_w(inifile, pt, galbias, gsp, wig, linear_rsd,
                             shot_noise);
  auto triangles = tayf::get_triangles(3, lmax);
  // Randomize triangles to sample uniformly multipole space before
  // cluster time limit.
  auto triangles_w = tayf::random_choice(triangles, triangles.size());

  std::cout << "# photo=["
            << params.zp_min << ", "
            << params.zp_max << "]" << std::endl;
  std::cout << "# shot_noise=" << shot_noise << std::endl;
  std::cout << "#" << std::endl;
  sn_w.compute(triangles_w, win);
}

void
run_spec(const std::string inifile,
         const std::shared_ptr<cosmox::Perturbations> pt,
         const std::shared_ptr<cosmox::Wigner> wig,
         const bool linear_rsd,
         const int lmax,
         const WindowType wtype)
{
  double zmin;
  double zmax;
  double shot_noise;
  std::shared_ptr<cosmox::GalaxyBias> galbias;
  switch (wtype)
    {
    case WindowType::kSpecLow:
      zmin = 0.4;
      zmax = 0.6;
      shot_noise = 1.45e-5;
      galbias = std::make_shared<tayf::DjsBias>(1.02);
      break;
    case WindowType::kSpecHigh:
      zmin = 0.90;
      zmax = 1.10;
      shot_noise = 1.68e-7;
      galbias = std::make_shared<tayf::EuclidBias>();
      break;
    case WindowType::kSpecHighNarrow:
      zmin = 0.99;
      zmax = 1.01;
      shot_noise = 1.68e-6;
      galbias = std::make_shared<tayf::EuclidBias>();
      break;
    default:
      throw tayf::TayfException("Not implemented", __FILE__, __LINE__);
    }
  auto win = std::make_shared<tayf::WindowSpec>(zmin, zmax);
  auto gsp = std::make_shared<cosmox::GeneralSpectra>(pt, galbias);
  tayf::SignalVarianceW sn_w(inifile, pt, galbias, gsp, wig, linear_rsd,
                             shot_noise);
  auto triangles = tayf::get_triangles(3, lmax);
  // Randomize triangles to sample uniformly multipole space before
  // cluster time limit.
  auto triangles_w = tayf::random_choice(triangles, triangles.size());

  std::cout << "# spec=["
            << zmin << ", "
            << zmax << "]" << std::endl;
  std::cout << "# shot_noise=" << shot_noise << std::endl;
  std::cout << "#" << std::endl;
  sn_w.compute(triangles_w, win);
}

/** @brief Set common specifications and dispatch. */
void
dispatch(const std::string inifile, const int lmax, const WindowType wtype)
{
  auto pt = cosmox::Perturbations::create_perturbs (cosmox::kCosmoclass,
                                                    inifile);
  auto wig = std::make_shared<cosmox::Wigner>(1000);
  auto linear_rsd = false;

  switch (wtype)
    {
    case WindowType::kNoLow:
    case WindowType::kNoHigh:
      {
        run_nowindow(inifile, pt, wig, linear_rsd, lmax, wtype);
        break;
      }
    case WindowType::kPhotoLow:
    case WindowType::kPhotoHigh:
      {
        run_photo(inifile, pt, wig, linear_rsd, lmax, wtype);
        break;
      }
    case WindowType::kSpecLow:
    case WindowType::kSpecHigh:
    case WindowType::kSpecHighNarrow:
      {
        run_spec(inifile, pt, wig, linear_rsd, lmax, wtype);
        break;
      }
    default:
      throw tayf::TayfException("Not implemented", __FILE__, __LINE__);
    }
}

void
print_usage(std::ostream& out)
{
  out << "Usage: tayf [OPTION]... FILE\n";
}

void
print_help(std::ostream& out)
{
  print_usage(out);
  out << "Compute bispectrum and variance given configuration FILE."
      << std::endl;
  out << "Example: tayf -l 200 -w photo-low class.ini" << std::endl;
  out << std::endl;
  const char *msg =
      "Options:\n"
      "-h         Print this help message\n"
      "-l N       Set maximum multipole (default: 100)\n"
      "-w TYPE    Window function type: {photo-low | photo-high |\n"
      "                                  spec-low | spec-high |\n"
      "                                  spec-high-narrow |\n"
      "                                  no-low | no-high}\n"
      "           (default: no-low)";
  out << msg << std::endl;
  out << std::endl;
  out << "FILE is a Class (the cosmological code) .ini file. Output is \n"
      << "printed to stdout as a function of (l1, l2, l3) multipole\n"
      << "triangles whose order may be random." << std::endl;
}

int
main(int argc, char** argv)
{
  int lmax = 100;
  std::string wval = "";
  auto wtype = WindowType::kNoLow;
  int c;

  opterr = 0;

  // -h exits immediately, leave it as the first parsed option.
  while ((c = getopt (argc, argv, "hl:w:")) != -1)
    switch (c)
      {
      case 'h':
        print_help(std::cout);
        exit(EXIT_SUCCESS);
      case 'l':
        lmax = std::stoi(optarg);
        break;
      case 'w':
        wval = optarg;
        if (!wval.compare("no-low"))
          continue;
        else if (!wval.compare("no-high"))
          wtype = WindowType::kNoHigh;
        else if (!wval.compare("photo-low"))
          wtype = WindowType::kPhotoLow;
        else if (!wval.compare("photo-high"))
          wtype = WindowType::kPhotoHigh;
        else if (!wval.compare("spec-low"))
          wtype = WindowType::kSpecLow;
        else if (!wval.compare("spec-high"))
          wtype = WindowType::kSpecHigh;
        else if (!wval.compare("spec-high-narrow"))
          wtype = WindowType::kSpecHighNarrow;
        else
          {
            fprintf (stderr,
                     "Option -%c: the argument you passed is not allowed\n",
                     optopt);
            exit(EXIT_FAILURE);
          }
        break;
      case '?':
        if ((optopt=='l') || (optopt=='w'))
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        exit(EXIT_FAILURE);
      default:
        abort();
      }

  if (argv[optind]==nullptr)
    {
      print_usage(std::cerr);
        exit(EXIT_FAILURE);
    }
  std::string inifile = argv[optind];

  dispatch(inifile, lmax, wtype);

  return 0;
}
