// utils.h -- Generic utilities.
//
// Copyright 2020 Francesco Montanari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef UTILS_H
#define UTILS_H

#include <memory>
#include <mutex>
#include <random>
#include <vector>

#include <cosmox/bispectrum_density.h>

#include "exceptions.h"

namespace tayf {

  std::mutex print_mutex;

  /** Return vector of triplets (l1, l2, l3) for which Gaunt factor is
   * non-zero and lmin <= l1 <= l2 <= l3 <= lmax.
   */
  std::vector<std::vector<int>>
  get_triangles(const int lmin, const int lmax)
  {
    std::vector<std::vector<int>> lgrid;
    for (auto l1=lmin; l1<=lmax; ++l1)
      for (auto l2=l1; l2<=lmax; ++l2)
        for (auto l3=l2; l3<=lmax; ++l3)
          if (!cosmox::is_gaunt_zero(l1, l2, l3))
            lgrid.push_back({l1, l2, l3});
    return lgrid;
  }

  /** @brief Choose a subsequence of elements randomly from an iterable. */
  template<class T>
  T random_choice(const T v, const int num)
  {
    if (v.size() < num)
      {
        auto msg = "random_choice: num must be smaller than iterable size";
        throw TayfException(msg, __FILE__, __LINE__);
      }
    auto x = T(v.begin(), v.end());
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(x.begin(), x.end(), g);
    return T(x.begin(), x.begin() + num);

  }

  /** @brief Print safely within multi-threaded region. */
  void
  print_sn(const int l1, const int l2, const int l3,
           const double signal, const double noise)
  {
    std::lock_guard<std::mutex> lock(print_mutex);
    std::cout << l1 << "  " << l2 << "  " << l3 << "  "
              << signal << "  " << noise << std::endl;
  }
} /* namespace tayf */

#endif  /* UTILS_H */
