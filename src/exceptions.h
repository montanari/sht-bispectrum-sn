// exceptions.h -- Exceptions handling

// Copyright 2020 Francesco Montanari

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

namespace tayf {

  /**
   * @brief Tayf exception handling class.
   *
   */
  class TayfException : public std::runtime_error {
  public:
    /** Use this constructor to throw an exception as usual. */
    TayfException(const std::string &msg);

    /** Use this constructor to display file and line number where the
     * exception is thrown. The message is displayed even without
     * catching it. Typical use:
     *
     *  \code{.cpp}
     *  throw TayfException("an error occurred", __FILE__, __LINE__);
     *  \endcode
     */
    TayfException(const std::string &msg, const char *file, int line);

    ~TayfException();

    /** @brief Error message.  */
    const char *what() const throw();

  private:
    std::string omsg;
  };

  TayfException::TayfException(const std::string &msg) :
    std::runtime_error(msg)
  {
    std::ostringstream o;
    o << msg;
    omsg = o.str();
  }

  TayfException::TayfException(const std::string &msg,
                                   const char *file,
                                   int line) :
    std::runtime_error(msg)
  {
    std::ostringstream o;
    o << file << ":" << line << ": " << msg;
    omsg = o.str();
  }

  TayfException::~TayfException() throw()
  {
  }

  const char *
  TayfException::what() const throw()
  {
    return omsg.c_str();
  }

} /* namespace tayf */

#endif  /* EXCEPTIONS_H */
