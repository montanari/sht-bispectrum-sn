FROM cosmox

RUN apt-get update
RUN apt-get install -y cmake
RUN apt-get clean

COPY ./ /usr/src/sht-bispectrum-sn/
WORKDIR /usr/src/sht-bispectrum-sn/
RUN mkdir _build && cd _build && cmake .. -DCMAKE_INSTALL_PREFIX=../_install \
    && make && make install
ENV LD_LIBRARY_PATH="/usr/local/lib:${LD_LIBRARY_PATH}"
ENTRYPOINT ["/usr/src/sht-bispectrum-sn/_install/bin/tayf"]
CMD ["/usr/src/sht-bispectrum-sn/cosmoclass.ini"]
RUN ldconfig
